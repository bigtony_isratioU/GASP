from gasp import *

begin_graphics()
def draw_face(x,y):
    Circle((x, y), 40)
    Circle((x - 15, y + 25), 5)
    Circle((x + 15, y + 25) ,5)
    Line((x - 10, y - 10), (x + 10, y - 10))
    Line((x - 10, y - 10), (x - 10, y + 20))
    Line((x - 20, y + 30), (x - 10, y + 30))
    Line((x + 10, y + 30), (x + 20, y + 30))
    Arc((x, y), 30, 225, 315)
    Arc((x - 35, y), 20, 90, 270)
    Arc((x + 35, y), 20, 270, 450)

draw_face(540,100)
update_when('key_pressed')
end_graphics()
